/**
 * An enumerator of list of area units.
 * @author  Parinvut Rochanavedya 
 * @version 2015.03.10
 */
public enum Area implements Unit{
	/**Unit Lists*/
	SQUARE_METER("Sq. Meter", Math.pow(1.0,2)),
	SQUARE_KILOMETER("Sq. Kilometer", Math.pow(1000.0,2)),
	SQUARE_CENTIMETER("Sq. Centimeter", Math.pow(0.01,2)),
	SQUARE_MILE("Sq. Mile", Math.pow(1609.344,2)),
	SQUARE_FOOT("Sq. Foot",Math.pow(0.3048,2)),
	SQUARE_WA("Sq. Wa (ตารางวา)", Math.pow(2.0,2));
	
	/**Unit name*/
	private String name;
	
	/**Unit value based on sq. meter*/
	private double value;
	
	/**
	 * Constructor
	 * @param name: Unit name
	 * @param value: Unit value (sq. meter)
	 */
	private Area(String name, double value){
		this.name = name;
		this.value = value;
	}

    /**
     * Convert the unit
     * @return the converted amount
     */
	public double convertTo(double amt, Unit unit) {
		return (amt*getValue())/unit.getValue();
	}

	/**
	 * Get the value based on sq. meter
	 * @return the value
	 */
	public double getValue() {
		return value;
	}
	
	/**
	 * Get the area unit's name
	 * @return area unit's name
	 */
	public String toString(){
		return name;
	}
}
