/**
 * An enumerator of list of length units.
 * @author  Parinvut Rochanavedya 
 * @version 2015.03.10
 */
public enum Length implements Unit{
	/**Unit Lists*/
	METER("Meter", 1.0),
	KILOMETER("Kilometer", 1000.0),
	CENTIMETER("Centimeter", 0.01),
	MILE("Mile", 1609.344),
	FOOT("Foot",0.3048),
	WA("Wa (��)", 2.0);
	
	/**Unit name*/
	private String name;
	
	/**Unit value based on meter*/
	private double value;
	
	/**
	 * Constructor
	 * @param name: Unit name
	 * @param value: Unit value (meter)
	 */
	private Length(String name, double value){
		this.name = name;
		this.value = value;
	}

    /**
     * Convert the unit
     * @return the converted amount
     */
	public double convertTo(double amt, Unit unit) {
		return (amt*getValue())/unit.getValue();
	}

	/**
	 * Get the value based on meter
	 * @return the value
	 */
	public double getValue() {
		return value;
	}
	
	/**
	 * Get the length unit's name
	 * @return length unit's name
	 */
	public String toString(){
		return name;
	}
}
