import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import javax.swing.JMenuItem;

/**
 * User interface of unit conversion
 * Converts the unitw of length
 * @author  Parinvut Rochanavedya 
 * @version 2015.03.10
 */
public class ConverterUI extends JFrame {

	/**The unit converter*/
	private UnitConverter unitconverter;
	
	/**Textfields Inputs*/
	private JTextField from;
	private JTextField to;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConverterUI frame = new ConverterUI(new UnitConverter());
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Constructor
	 */
	public ConverterUI(UnitConverter uc) throws Exception{
		this.unitconverter = uc;
		
		setTitle("Unit Converter");
	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 100);
		getContentPane().setLayout(null);
		
		
		
		//3 km = 3000 m
		//how to turn 3 into 3000?
		//System.out.print(uc.convert(3.0, Length.KILOMETER, Length.METER));
		
		initComponents();
	}
	
	/**
	 * Initialize the GUI components
	 */
	public void initComponents() {
		from = new JTextField();
		from.setBounds(10, 11, 105, 20);
		getContentPane().add(from);
		from.setColumns(10);
		
		JComboBox fromUnit = new JComboBox();
		fromUnit.setMaximumRowCount(100);
		fromUnit.setModel(new DefaultComboBoxModel(Length.values()));
		fromUnit.setBounds(121, 11, 105, 20);
		getContentPane().add(fromUnit);
		
		JLabel label = new JLabel("=");
		label.setBounds(235, 14, 25, 14);
		getContentPane().add(label);
		
		to = new JTextField();
		to.setEditable(false);
		to.setBounds(250, 11, 105, 20);
		getContentPane().add(to);
		to.setColumns(10);
		
		JComboBox toUnit = new JComboBox();
		toUnit.setMaximumRowCount(100);
		toUnit.setModel(new DefaultComboBoxModel(Length.values()));
		toUnit.setBounds(363, 11, 105, 20);
		getContentPane().add(toUnit);
		
		JButton btnNewButton = new JButton("Convert");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					double varFrom = Double.parseDouble(from.getText());
					Unit varFromUnit = ((Unit)fromUnit.getSelectedItem());
					Unit varToUnit  = ((Unit)toUnit.getSelectedItem());
					
					double varTo = unitconverter.convert(varFrom, varFromUnit, varToUnit);
					to.setForeground(Color.BLACK);
					to.setText(String.format("%.5g", varTo));
				} catch (java.lang.NumberFormatException e){
					to.setForeground(Color.RED);
					to.setText("Invalid");
				}
				
				
			}
		});
		btnNewButton.setBounds(488, 10, 89, 23);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Clear");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				from.setText("");
				to.setForeground(Color.BLACK);
				to.setText("");
			}
		});
		btnNewButton_1.setBounds(585, 10, 89, 23);
		getContentPane().add(btnNewButton_1);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Unit Type");
		menuBar.add(mnNewMenu);
		
		JRadioButtonMenuItem length_type = new JRadioButtonMenuItem("Length");
		length_type.setSelected(true);
		mnNewMenu.add(length_type);
		
		JRadioButtonMenuItem area_type = new JRadioButtonMenuItem("Area");
		mnNewMenu.add(area_type);
		
		JRadioButtonMenuItem weight_type = new JRadioButtonMenuItem("Weight");
		mnNewMenu.add(weight_type);
		
		
		
		
		JSeparator separator = new JSeparator();
		mnNewMenu.add(separator);
		
		JMenuItem exit = new JMenuItem("Exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnNewMenu.add(exit);
		
		length_type.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				length_type.setSelected(true);
				area_type.setSelected(false);
				weight_type.setSelected(false);
				
				fromUnit.setModel(new DefaultComboBoxModel(Length.values()));
				toUnit.setModel(new DefaultComboBoxModel(Length.values()));
				
				from.setText("");
				to.setForeground(Color.BLACK);
				to.setText("");
				
			}
		});
		
		area_type.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				area_type.setSelected(true);
				length_type.setSelected(false);
				weight_type.setSelected(false);
				
				fromUnit.setModel(new DefaultComboBoxModel(Area.values()));
				toUnit.setModel(new DefaultComboBoxModel(Area.values()));
				
				from.setText("");
				to.setForeground(Color.BLACK);
				to.setText("");
			}
		});
		
		weight_type.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				weight_type.setSelected(true);
				length_type.setSelected(false);
				area_type.setSelected(false);
				
				fromUnit.setModel(new DefaultComboBoxModel(Weight.values()));
				toUnit.setModel(new DefaultComboBoxModel(Weight.values()));
				
				from.setText("");
				to.setForeground(Color.BLACK);
				to.setText("");
			}
		});
	}
	
}
