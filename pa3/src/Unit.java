/**
 * A property of every units.
 * @author  Parinvut Rochanavedya 
 * @version 2015.03.10
 */
public interface Unit {
	/**
	 * Convert current unit to another unit
	 * @return the converted amount
	 */
	double convertTo(double amt, Unit unit);
	/**
	 * Get the value
	 * @return value
	 */
	double getValue();
	/**
	 * Get the unit's name
	 * @return unit's name
	 */
	String toString();
}
