/**
 * An enumerator of list of weight units.
 * @author  Parinvut Rochanavedya 
 * @version 2015.03.10
 */
public enum Weight implements Unit{
	/**Unit Lists*/
	GRAM("Gram", 1.0),
	KILOGRAM("Kilogram", 1000.0),
	MICROGRAM("Microgram", 0.000001),
	TON("Ton",907200.0),
	POUNCE("Pounce",453.6),
	KEET("Keet (�մ)", 100.0);
	
	/**Unit name*/
	private String name;
	
	/**Unit value based on gram*/
	private double value;
	
	/**
	 * Constructor
	 * @param name: Unit name
	 * @param value: Unit value (gram)
	 */
	private Weight(String name, double value){
		this.name = name;
		this.value = value;
	}

    /**
     * Convert the unit
     * @return the converted amount
     */
	public double convertTo(double amt, Unit unit) {
		return (amt*getValue())/unit.getValue();
	}

	/**
	 * Get the value based on gram
	 * @return the value
	 */
	public double getValue() {
		return value;
	}
	
	/**
	 * Get the weight unit's name
	 * @return weight unit's name
	 */
	public String toString(){
		return name;
	}
}
